import { convert } from "../src/convert"

// Regular input as given in question
let inp1 = {"2020-01-01":4, "2020-01-02":4, "2020-01-03":6, "2020-01-04":8, "2020-01-05":2, "2020-01-06":-6, "2020-01-07":2, "2020-01-08":-2}
let out1 = {"Mon":-6,"Tue":2,"Wed":2,"Thu":4,"Fri":6,"Sat":8,"Sun":2}

// Regular input as given in question
let inp2 = {"2020-01-01":6, "2020-01-04":12, "2020-01-05":14, "2020-01-06":2, "2020-01-07":4}
let out2 = {"Mon":2,"Tue":4,"Wed":6,"Thu":8,"Fri":10,"Sat":12,"Sun":14}

// Input without Monday
let inp3 = {"2020-01-01":6, "2020-01-04":12, "2020-01-05":14, "2020-01-07":4}
let out3 = -1

// key-value pairs exceeding range in date or value are ignored
// Date
let inp4 = {"2220-01-01":6, "2020-01-04":12, "2020-01-05":14, "2020-01-06":2, "2020-01-07":4}
let out4 = {"Mon":2,"Tue":4,"Wed":8,"Thu":8,"Fri":10,"Sat":12,"Sun":14}

// Value
let inp5 = {"2020-01-01":10, "2020-01-04":2, "2020-01-05":14, "2020-01-06":2, "2020-01-07":14000000}
let out5 = {"Mon":2,"Tue":6,"Wed":10,"Thu":4,"Fri":3,"Sat":2,"Sun":14}

// test("checking 1st input", () => {
//     expect(convert(inp1)).toBe(out1);
//   });

describe('Convert', () => {
  it('convert1', () => {
    let o1 = convert(JSON.stringify(inp1))
    expect(o1).toStrictEqual(out1)
  })
})

describe('Convert', () => {
  it('convert2', () => {
    let o2 = convert(JSON.stringify(inp2))
    expect(o2).toStrictEqual(out2)
  })
})

describe('Convert', () => {
  it('convert3', () => {
    let o3 = convert(JSON.stringify(inp3))
    expect(o3).toStrictEqual(out3)
  })
})

describe('Convert', () => {
  it('convert4', () => {
    let o4 = convert(JSON.stringify(inp4))
    expect(o4).toStrictEqual(out4)
  })
})

describe('Convert', () => {
  it('convert5', () => {
    let o5 = convert(JSON.stringify(inp5))
    expect(o5).toStrictEqual(out5)
  })
})


// describe('is working', () => {
//   it('should work', () => {
//     expect(true).toBeTruthy();
//   })
// })