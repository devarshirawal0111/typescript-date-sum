import { convert } from "./convert"

const readline = require('readline');

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

rl.question('Enter a sample date: ', function(answer) {
    let result = convert(answer);
    console.log(JSON.stringify(result))
    rl.close();
});
// rl.close()
