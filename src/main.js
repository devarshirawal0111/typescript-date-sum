"use strict";
exports.__esModule = true;
var convert_1 = require("./convert");
var readline = require('readline');
var rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});
rl.question('Enter a sample date: ', function (answer) {
    var result = convert_1.convert(answer);
    console.log(JSON.stringify(result));
    rl.close();
});
// rl.close()
