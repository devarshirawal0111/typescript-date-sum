"use strict";
exports.__esModule = true;
exports.convert = void 0;
function convert(userinput) {
    var obj = JSON.parse(userinput);
    // array that matches day with week-days
    var days = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
    // final output JSON
    var result = {
        "Mon": 0,
        "Tue": 0,
        'Wed': 0,
        'Thu': 0,
        'Fri': 0,
        'Sat': 0,
        'Sun': 0
    };
    // array to count sum of values for each day
    var temp_result = [0, 0, 0, 0, 0, 0, 0];
    //array to count # of inputs of each week-day
    var temp_result_count = [0, 0, 0, 0, 0, 0, 0];
    for (var _i = 0, _a = Object.entries(obj); _i < _a.length; _i++) {
        var _b = _a[_i], key = _b[0], value = _b[1];
        var date = new Date(key); // Convert string value to Date object
        var year = date.getFullYear(); // Extract year from Date
        var day = (date.getDay()); // Extract day from Date
        var amt = +value; // Convert string value of input Value
        // Checking Year input range
        if ((year > 1969 && year < 2099) || key == '2100-01-01') {
            // Checking Value range
            if (value > -1000001 && value < 1000001) {
                // Mapping day with week-day and suming with previous value
                temp_result[day % 7] += amt;
                // Incrementing week-day count
                temp_result_count[day % 7]++;
            }
            else {
                console.log('Value in (', key, ') is out of range');
            }
        }
        else {
            console.log('Date (', key, ') is out of range');
        }
    }
    // console.log(temp_result_count)
    // console.log(temp_result)
    // Taking average for missing values
    // 0->Sunday, 1->Monday and so on.
    if (temp_result_count[0] == 0 || temp_result_count[1] == 0) {
        console.log('No Sunday or Monday present');
        // console.log(temp_result)
        return -1;
    }
    else {
        // Starting from Tuesday, checking temp_result_count array for 
        // figuring out which days' inputs are missing.
        // Logic:
        //   i) Check if current day is missing, if so, check immediate neighbours
        //  ii) If any of the immediate neighbours have missing values, take another step in both direction
        // iii) If still has a missing value on either of the neighbours, use Monda and Sunday for average
        for (var i = 2; i < 7; i++) {
            if (temp_result_count[i] == 0) {
                if (temp_result_count[i - 1] == 0 || temp_result_count[i + 1] == 0) {
                    if (temp_result_count[i - 2] == 0 || temp_result_count[i + 2] == 0) {
                        temp_result[i] = (temp_result[0] + temp_result[1]) / 2;
                        temp_result_count[i]++;
                    }
                    else {
                        temp_result[i] = (temp_result[i - 2] + temp_result[i + 2]) / 2;
                        temp_result_count[i]++;
                    }
                }
                else {
                    temp_result[i] = (temp_result[i - 1] + temp_result[i + 1]) / 2;
                    temp_result_count[i]++;
                }
            }
        }
        // Mapping array with sum of values with days
        temp_result.map(function (val, index) {
            result[days[index]] = val;
        });
        return result;
    }
}
exports.convert = convert;
